// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBjsEXmLI-4kOcz793QZh9ElN_iSHlO74M",
    authDomain: "ng-firebase-dev.firebaseapp.com",
    databaseURL: "https://ng-firebase-dev.firebaseio.com",
    projectId: "ng-firebase-dev",
    storageBucket: "ng-firebase-dev.appspot.com",
    messagingSenderId: "644800494252"
  }
};
